helper = {
    vars: {
        itemIcon: {
            'yt': {
                src: 'web/img/play.png',
                alt: 'YouTube'
            },
            'local': {
                src: 'web/img/lp.png',
                alt: 'HDD'
            },
            default: {
                src: 'web/img/note-gray.png',
                alt: 'Note'
            }
        },
        alerts: {
            'max_items': {
                status: 'CHYBA',
                msg: 'Byl dosažen maximální počet položek v playlistu.',
                color: 'danger'
            },
            'item_added': {
                status: 'ZPRÁVA',
                msg: 'Položka byla úspěšně přidána do playlistu.',
                color: 'warning'
            },
            'item_not_added': {
                status: 'CHYBA',
                msg: 'Položka nebyla přidána do playlistu.',
                color: 'danger'
            },
            'file_up_success': {
                status: 'ÚSPĚCH',
                msg: 'Soubor byl úspěšně nahrán na server.',
                color: 'success'
            },
            'file_up_error': {
                status: 'CHYBA',
                msg: 'Během nahrávání souboru nastal problém.',
                color: 'danger'
            },
            'low_credit': {
                status: 'CHYBA',
                msg: 'Nedostatečný kredit.',
                color: 'danger'
            },
            'item_paid': {
                status: 'ZPRÁVA',
                msg: 'Platba proběhla úspěšně.',
                color: 'warning'
            },
            'pay_error': {
                status: 'Error',
                msg: 'Platba se nepodařila',
                color: 'danger'
            }
        }
    },
    ctrl: {
        convertSecToDur: function(sec) {
            var m = Math.floor(sec/60);
            var s = sec % 60;
            if(s < 10) {s = '0'+s;}
            return m + ':' + s;
        },
        convertTime: function(string) {
            var raw = string.substr(2);
            var hours = raw.replace('H', ':');
            var minutes = hours.replace('M', ':');
            var parsed = minutes.replace('S', '');
            var splited = parsed.split(':');
            var pMinutes = '';
            var pSeconds = '';
            var final = '';
            if(splited.length === 3) {
                if (parseInt(splited[1]) < 10) {
                    pMinutes = '0' + splited[1];
                } else {
                    pMinutes = splited[1];
                }
                if (parseInt(splited[2]) < 10) {
                    pSeconds = '0' + splited[2];
                } else {
                    pSeconds = splited[2];
                }
                final = splited[0]+':'+pMinutes+':'+pSeconds;
            } else if(splited.length === 2) {
                pMinutes = splited[0];
                if (parseInt(splited[1]) < 10) {
                    pSeconds = '0' + splited[1];
                } else {
                    pSeconds = splited[1];
                }
                if(splited[1] !== '') {
                    final = pMinutes+':'+pSeconds;
                } else {
                    final = pMinutes+':00';
                }
            } else if(splited.length === 1) {
                final = '00:'+splited[0];
            }
            return final;
        },
        getPlayerSize: function (dir) {
            var width = 640;
            var height = 480;
            var response;
            if(dir === 'height') {
                response = height;
            }
            else if (dir === 'width') {
                response = width;
            }
            else {
                response = null;
            }

            return response;
        },
        showAlert: function (alert_type) {
            var data = helper.vars.alerts[alert_type];
            var response;
            if (data!== undefined) {
                helper.ctrl.raiseAlert(data.status, data.msg, data.color);
                response = true;
            }
            else {
                response = false;
                console.log('showAlert failed: No data available for given key ('+alert_type+')');
            }

            return response;
        },
        sleep: function(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        },
        raiseAlert: async function (status, msg, color) {
            var tag = $('#alert-field-tpl');
            var target = tag.clone().prependTo('body');
            target.removeAttr('id');
            target.addClass('alert-'+color);
            target.find('.alert-status').html(status+': ');
            target.find('.alert-text').html(msg);
            target.removeClass('hidden');
            await helper.ctrl.sleep(3000);
            target.remove();
        },
        fillMetadata: function (file) {
            jsmediatags.read(file, {
                onSuccess: function(tag) {
                    var tags = tag.tags;
                    $('#media_album').val(tags.TALB.data); //album
                    $('#media_title').val(tags.TIT2.data); //title
                    $('#media_artist').val(tags.TPE1.data); //artist
                },
                onError: function(error) {
                    console.log({fillMetadataError: error});
                }
            });
        }
    }
};

cashdesk = {
    vars: {
        amount: 0,
        currency: 'CRD',
        cost: 1,
        paid: false,
        done: null
    },
    ctrl: {
        pay: function (amount, currency, callback = function () {}, data = null) {
            cashdesk.ctrl.manipulateCredits(amount, currency).done(function () {
                var paid = cashdesk.vars.paid;
                helper.ctrl.showAlert((paid === true) ? 'item_paid' : 'pay_error');
                if(paid) {
                    callback(data);
                }
            });
        },
        manipulateCredits: function (amount, currency) {
            return $.post({
                url: 'pay',
                data: {
                    amount: amount,
                    currency: currency,
                    _csrf: yii.getCsrfToken()
                },
                success: function(data) {
                    if(data != false) {
                        cashdesk.vars.paid = true;
                        cashdesk.ctrl.changeCreditAmount(data);
                    }
                },
                callback: function() {
                    cashdesk.vars.done = true;
                },
                dataType: 'json'
            });
        },
        changeCreditAmount: function (amount) {
            var tag = $('#credit-bar .money-amount');
            tag.html(amount);
        }
    }
};

mPlayer = {
    params: {
        class: 'plyr',
        id: 'player',
        poster: 'web/img/album.png'
    },
    vars: {
            container: '.album-view .album',
            width: helper.ctrl.getPlayerSize('width'),
            height: helper.ctrl.getPlayerSize('height'),
            playing: false
    },
    ctrl: {
        init: function () {
            var video = '<video poster="'+mPlayer.params.poster+'" id="'+mPlayer.params.id+'" autoplay="true">';
            mPlayer.video = $(mPlayer.vars.container).append(video);
            mPlayer.self = new Plyr('#player', {
                controls: ['play-large', 'current-time', 'volume', 'fullscreen'],
                loop: {active: false},
                autoplay: false,
                keyboard: {focused: false, global: false}
            });
            mPlayer.self.stop();
        },
        play: function (data) {
            if(data.type === 'yt') {
                mPlayer.self.source = {
                    type: 'video',
                    sources: [{
                        src: data.id,
                        provider: 'youtube'
                    }]
                };
            }

            else if (data.type === 'local') {
                mPlayer.self.source = {
                    type: 'audio',
                    title: data.song,
                    sources: [{
                        src: 'web/media/'+data.id+'.mp3',
                        type: 'audio/mp3',
                        size: mPlayer.params.width
                    }]
                };
            }
            var pl_item = mPlayer.ctrl.getActiveItem();
            if(pl_item !== null) {
                pl_item.addClass('active');
                var div = pl_item.find('.one-row-list');
                div.addClass('active');
            }
            mPlayer.self.play();
            mPlayer.vars.playing = true;
        },
        stop: function () {
            mPlayer.self.stop();
            mPlayer.self.source = {
                type: 'video',
                title: 'Example title',
                poster: mPlayer.params.poster,
                sources: [{
                    src: '',
                    type: 'video/mp4'
                }]
            };
            mPlayer.self.poster = mPlayer.params.poster;
            mPlayer.vars.playing = false;
            console.log('Player stopped');
        },
        remove: function () {
            $('#'+mPlayer.params.id).remove();
        },
        autoplay: function () {
            var last = $(playlist.vars.container+' > li.active');
            playlist.ctrl.remove(last.attr('uid'), last.attr('count'));
            var curr = mPlayer.ctrl.getActiveItem();

            if(curr !== null && curr.length !== 0 && curr !== last){
                curr.addClass('active');
                curr.find('.one-row-list').addClass('active');
                var curr_data = playlist.items[curr.attr('count')];
                mPlayer.ctrl.play(curr_data);
            }
            else {
                mPlayer.ctrl.stop();
            }
            console.log('autoplay activated');
        },
        getActiveItem: function () {
            var selector = playlist.vars.container+' > li[uid]:first-child:not(.active)';
            var item = $(selector);

            if(item.length === 0) {
                item = null;
            }

            return item;
        }
    }
};

search = {
    variables: {
        mode: 'yt'
    },
    items: {},
    ctrl: {
        find: function(keyword) {
            search.ctrl.reset();
            if(search.variables.mode === 'yt') {
                ytapi.ctrl.find(keyword);
            }
            else if (search.variables.mode === 'local') {
                local.ctrl.search(keyword);
            }
        },
        add: function (data) {
            search.items[data.id] = data;
            var el = $('.dark-content > ul > .hid-tpl').clone().appendTo('.dark-content > ul');
            el.removeClass('hid-tpl');
            el.id = data.type+'-'+data.id;
            var icon_el = el.find('.one-row-list .note-place img');

            if(data.type === 'yt') {
                icon_el.attr('src', helper.vars.itemIcon.yt.src);
                icon_el.attr('alt', helper.vars.itemIcon.yt.alt);
            }
            else if(data.type === 'local') {
                icon_el.attr('src', helper.vars.itemIcon.local.src);
                icon_el.attr('alt', helper.vars.itemIcon.local.alt);
            }
            el.attr('uid', data.id);
            el.attr('type', data.type);
            el.find('.artist').html(data.artist);
            el.find('.song').html(data.title);
            el.find('.time-place .length').html(data.duration);
        },
        reset: function () {
            search.items = {};
            $('.dark-content > ul > li:not(.hid-tpl)').each(function (i, e) {
                e.remove();
            });
        },
        switchMode: function (type) {
            search.variables.mode = type;
            search.ctrl.find($('#search-input').val());
        }
    }
};

local = {
    items: {},
    ctrl: {
        init: function () {
            $.ajax('get-local', {
                method: 'post',
                data: {_csrf: yii.getCsrfToken()},
                dataType: 'json',
                success: function (data) {
                    for(item in data)
                    {
                        data[item].type = 'local';
                        data[item].src = 'media/'+data[item].src;
                    }
                    local.items = data;
                }
            });
        },
        search: function (keyword) {
            search.items = {};
            var pattern;
            if(keyword.length === 0) {
                pattern = '(.*)';
            }
            else
            {
                pattern = '('+keyword+')(.*)';
            }
            var regex = new RegExp(pattern, 'i');
            for (item in local.items) {
                var result = false;
                var match = null;
                if (regex.test(local.items[item].media_title)) {
                    result = true;
                }
                else if (regex.test(local.items[item].media_artist)) {
                    result = true;
                }
                if(result === true) {
                    search.ctrl.add(local.items[item]);
                }
            }
        }
    }
};

ytapi = {
    variables: {
        googleApiKey: 'AIzaSyAbUlM0WL94efNuVF1ZN2CtWPbORzDuvw4'
    },
    items: {},
    ctrl: {
        find: function (searchTerm) {
            var url = 'https://www.googleapis.com/youtube/v3/search';
            var params = {
                part: 'snippet',
                type: 'video',
                key: ytapi.variables.googleApiKey,
                maxResults: 30,
                q: searchTerm
            };
            $.getJSON(url, params, function (response) {
                search.ctrl.reset();
                ytapi.ctrl.handleResponse(response);
            });
        },
        handleResponse: function (response) {
            var ids = [];
            for(item in response.items) {
                var params = {
                    id: response.items[item].id.videoId,
                    title: response.items[item].snippet.title,
                    artist: response.items[item].snippet.channelTitle,
                    type: 'yt',
                    src: ''
                };
                ids.push(params.id);
                ytapi.items[params.id] = params;
            }
            ytapi.ctrl.getInfo(ids);
        },
        getInfo: function(ids) {
            var id_str = '';
            for(i=0; i <= ids.length; i++) {
                id_str += ((i === 0) ? '' : ',')+ids[i];
            }

            var url = 'https://www.googleapis.com/youtube/v3/videos';
            var params = {
                part: 'contentDetails',
                key: ytapi.variables.googleApiKey,
                id: id_str
            };

            $.getJSON(url, params, function (response) {
                var items = response.items;
                for(item in response.items) {
                    if(ytapi.items[response.items[item].id]){
                        var duration = helper.ctrl.convertTime(response.items[item].contentDetails.duration);
                        ytapi.items[response.items[item].id].duration = duration;
                    }
                    search.ctrl.add(ytapi.items[response.items[item].id]);
                    search.items[response.items[item].id] = ytapi.items[response.items[item].id];
                }

            });

        }
    }
};

playlist = {
    vars: {
        container: '.playlist-content > ul',
        count: 0
    },
    items: {},
    ctrl: {
        init: function () {
            var lost_string = localStorage.getItem('playlist');
            if(lost_string !== null) {
                var lost = JSON.parse(lost_string);
                for(item in lost) {
                    playlist.ctrl.add(lost[item], true);
                }
            }
        },
        add: function (data, def = false) {
            var mod_free = (settings.mode.value === 'a') ? true : false;
            var max_items = playlist.vars.count < parseInt(settings.playlist_item_count.value);

            if(!mod_free || (mod_free && max_items))
            {
                if (!mod_free && !def) {
                    var payment = cashdesk.ctrl.pay(-1, 'CRD', playlist.ctrl._add, data);
                }

                if(mod_free || def) {
                    playlist.ctrl._add(data);
                }
            }
            else
            {
                helper.ctrl.showAlert('max_items');
            }
        },
        _add: function (data) {
                var target = $(playlist.vars.container);
                var play_mode = settings.play_mode.value;
                var el = $(playlist.vars.container+' > .hid-tpl').clone();
                var order = playlist.vars.count + 1;

                if(play_mode === 'append') {
                    el.appendTo(target);
                }
                else if (play_mode === 'prepend') {
                    el.prependTo(target);
                }
                else if (play_mode === 'hybrid') {
                    var active = mPlayer.ctrl.getActiveItem();
                    if(active !== null) {
                        el.insertAfter(active);
                    }
                    else {
                        el.prependTo(target);
                    }
                }

                if(mPlayer.self !== undefined && mPlayer.vars.playing === false) {
                    mPlayer.ctrl.play(data);
                }

                playlist.items[order] = data;

                localStorage.setItem('playlist', JSON.stringify(playlist.items));

                el.attr('uid', data.id);
                el.attr('type', data.type);
                el.find('.artist').html(data.artist);
                el.find('.song').html(data.title);
                el.find('.time-place .length').html(data.duration);
                el.removeClass('hid-tpl');
                el.find('.note-place img').attr('src', helper.vars.itemIcon[data.type].src);
                el.find('.note-place img').attr('alt', helper.vars.itemIcon[data.type].alt);

                if(settings.item_delete.value === 'b') {
                    el.find('.remove-song').remove();
                }

                playlist.ctrl.recount();
                el.attr('count', order);
                playlist.ctrl.reorder();

                helper.ctrl.showAlert('item_added');
        },
        remove: function (id, count) {
            delete playlist.items[count];
            localStorage.setItem('playlist', JSON.stringify(playlist.items));
            var item = $(playlist.vars.container+' > li[count='+count+']');
            if(item.hasClass('active')) {
                mPlayer.ctrl.stop();
            }
            item.remove();
            playlist.ctrl.recount();
            playlist.ctrl.reorder();
        },
        reorder: function () {
            var count = 0;
            var temp = {};

            $(playlist.vars.container+' > li[uid]').each(function () {

                temp[count] = playlist.items[$(this).attr('count')];
                $(this).attr('count', count);
                count++;
            });

            playlist.items = {};
            Object.assign(playlist.items, temp);
        },
        recount: function () {
          playlist.vars.count = Object.keys(playlist.items).length;
        },
        reset: function () {
            $(playlist.vars.container).empty();
            mPlayer.ctrl.stop();
            localStorage.removeItem('playlist');
        },
        canAdd : function () {
            var pl_el = $('.playlist-content > ul');
            var response = false;

            if(settings.mode.value === 'b')
            {
                response = true;
            }
            else if(playlist.items.length < settings.playlist_item_count.value)
            {
                response = true;
            }

            return response;
        }
    }
};
