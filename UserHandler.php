<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\web\IdentityInterface;
use app\repos\UserRepo;
use app\models\User;
use yii\web\UserEvent;
use yii\helpers\Url;

class UserHandler extends Component implements IdentityInterface
{
    public $ID;
    public $role;
    public $username;
    public $key;
    public $token;
    public $remember;
    private $_model;
    private $_repo;

    const EVENT_AFTER_LOGIN = 'after-login';    
    const LOGIN_EXPIRATION = 2592000; //3600 * 24 * 30

    public function __construct(User $user = NULL)
    {
        $this->setModel($user);
        $this->setRepo();
        $empty = $this->_isEmpty();

        $this->on(self::EVENT_AFTER_LOGIN, function ($event) {
            Yii::$app->setHomeUrl(Url::to(['site/home']));
        });

        if ($empty)
        {
            $this->setData($this->_model);
        }
    }

    /**
     * This method is called after the user is successfully logged in.
     * The default implementation will trigger the [[EVENT_AFTER_LOGIN]] event.
     * @param IdentityInterface $identity the user identity information
     * @param bool $cookieBased whether the login is cookie-based
     * @param int $duration number of seconds that the user can remain in logged-in status.
     * If 0, it means login till the user closes the browser or the session is manually destroyed.
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        $this->trigger(self::EVENT_AFTER_LOGIN, new UserEvent([
            'identity' => $identity,
            'cookieBased' => $cookieBased,
            'duration' => $duration,
        ]));
    }

    /**
     * Checks if instance is empty.
     * @return boolean
     */
    private function _isEmpty()
    {
        $response = false;
        if (!$this->ID)
        {
            $response = true;
        }
        elseif (!$this->username)
        {
            $response = true;
        }

        return $response;
    }

    /**
     * @return boolean
     */
    public function validateLogin()
    {
        $response = false;
        $data = $this->_repo->getCredentials($this->_model->username);

        if ($data)
        {
            $response = $this->_validatePassword($this->_model->pwd, $data['pwd']);
            $this->ID = $data['ID'];
        }

        return $response;
    }

    /**
     * Returns instance of this class filled with user data.
     * @param int $id
     * @return UserHandler
     */
    public static function findIdentity($ID)
    {
        $model = new User();
        $user = $model->getUser($ID);
        $identity = new UserHandler($user);

        return $identity;
    }

    /**
     * 
     * @param string $username
     * @return array
     */
    public static function findByUsername($username)
    {
        $model = new User;
        $repo = new UserRepo($model);
        $user = $repo->findByUsername($username);

        return $user;
    }

    /**
     * 
     * @param type $token
     * @param type $type
     * @return type
     */
    public static function findIdentityByAccessToken($token, $type = NULL)
    {
        return UserRepo::findOne(['token' => $token]);
    }

    public function getId()
    {
        return $this->ID;
    }

    public function getAuthKey()
    {
        return $this->key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->key === $authKey;
    }

    public function setModel($model = NULL)
    {
        if ($model && $model instanceof User)
        {
            $this->_model = $model;
        }
        else
        {
            $this->_model = new User();
        }
    }

    public function setRepo($repo = NULL, $model = NULL)
    {
        if ($repo && $repo instanceof UserRepo)
        {
            $this->_repo = $repo;
        }
        else
        {
            if ($model)
            {
                $this->setModel($model);
            }
            if ($this->_model && $this->_model instanceof User)
            {
                $this->_repo = new UserRepo($this->_model);
            }
            else
            {
                $this->setModel();
                $this->setRepo(NULL, $this->_model);
            }
        }
    }

    private function _fetchData($ID_user = NULL)
    {
        if ($ID_user)
        {
            $this->setData(['ID' => $ID_user]);
        }

        $data = $this->_repo->fetchUser($this->ID);

        $this->setData($data);
    }

    public function setData($user)
    {
        $response = NULL;

        if ($user instanceof User)
        {
            $this->ID = (int)$user->ID;
            $this->username = $user->username;
            $this->role = $user->role;
            $this->token = $user->token;
            $this->key = $user->key;
//WTF????
            if ($this->remember <> $user->remember)
            {
                $this->setRemember($user->remember);
            }
            else
            {
                $this->remember = $user->remember;
            }

            $response = true;
        }
        elseif (is_array($user))
        {
            $validate = $this->_checkUserArrayStructure($user);

            if ($validate)
            {
                $this->ID = (int)$user['ID'];
                $this->username = $user['username'];
                $this->role = $user['role'];
                $this->token = $user['token'];
                $this->key = $user['key'];
                $this->remember = $user['remember'];
                $response = true;
            }
            else
            {
                $response = false;
            }
        }

        return $response;
    }

    private function _checkUserArrayStructure()
    {
        return true;
    }

    public function validateUser(User $user)
    {
        return $user->validate();
    }

    private function _validatePassword($formPswd, $dbPswd)
    {
        $hashed = $this->_model->hashPswd($formPswd);

        return $hashed === $dbPswd;
    }

    private function setRemember()
    {
        $this->_repo->save(
            [
                'ID'       => $this->ID,
                'remember' => $this->remember,
            ]);
    }

    public function login()
    {
        $response = false;

        if ($this->validateLogin())
        {
            $this->_fetchData();
            $response = Yii::$app->user->login($this, ($this->remember ? self::LOGIN_EXPIRATION : 0));
        }
        $this->afterLogin($this, FALSE, self::LOGIN_EXPIRATION);
        return $response;
    }

    public function register($form)
    {
        $response = false;
        $this->_model->setAttributes($form, true);
        $valid = $this->_model->validate();
        if ($valid)
        {
            $response = $this->_repo->register();
        }

        return $response;
    }
}
